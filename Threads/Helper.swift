//
//  Helper.swift
//  Threads
//
//  Created by Pranav Bhandari on 8/8/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Helper {
    
     static let helper = Helper()
    
    // MARK: - Fetching Post Details
    func fetch_post_details()
    {
        dispatch_async(dispatch_get_main_queue()) {
            let url = NSURL(string:"https://thread-app.herokuapp.com/postdetails")
            let request = NSMutableURLRequest(URL: url!)
            //  request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let parameters : [String:AnyObject] = [ "proj_id" : selected_proj_id
            ]
            
            //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            
            Alamofire.request(.GET, url!, parameters: parameters).responseJSON
                { response in switch response.result {
               // case .Success(let JSON):
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        post_proj_details = json["proj_posts"].array!
                        print("45",post_proj_details)
                        let ans = json["proj_posts"][0]["post_media"].array
                        post_media_urls = ans!
                       let an1 = post_media_urls[0]["post_media_url"]
                        print("JSON: \(an1)")
                        self.fetch_user_details_for_post()
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let naviVC = storyBoard.instantiateViewControllerWithIdentifier("ThreadHomePage") as!ThreadsHomePageViewController
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.window?.rootViewController = naviVC
                        
                    }
                case .Failure(let error):
                    print(error)
                    }
            }
        }
    }
    
    func fetch_user_details_for_post()
    {
         dispatch_async(dispatch_get_main_queue()) {
            
            for(var i = 0; i < post_proj_details.count;i++)
            {
                let url = NSURL(string:"https://thread-app.herokuapp.com/usrdetails")
                let parameters : [String:AnyObject] = [ "usr_id" : post_proj_details[i]["post_usr_id"].string!
                ]
                
                //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
                
                
                Alamofire.request(.GET, url!, parameters: parameters).responseJSON
                    { response in switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            let usrdetails = JSON(value)
                            print(usrdetails["message"])
//                            print(usrdetails["message"][0]["usr_fname"] + " " + usrdetails["message"][0]["usr_lname"])
//                            post_user_created.append(usrdetails["message"][0]["usr_fname"] + " " + usrdetails["message"][0]["usr_lname"])
                        }
                    case .Failure(let error):
                        print(error)
                        
                        
                        }
                }
            }
                
          
        }
    }
}
