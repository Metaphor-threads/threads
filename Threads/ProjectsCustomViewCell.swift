//
//  ProjectsCustomViewCell.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/30/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class ProjectsCustomViewCell: UICollectionViewCell {

    @IBOutlet weak var profile_projects_img: UIImageView!
    @IBOutlet weak var profile_projects_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        
//        // Configure the view for the selected state
//    }
}
