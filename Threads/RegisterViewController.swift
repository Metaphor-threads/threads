//
//  RegisterViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/12/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class RegisterViewController: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    @IBOutlet weak var yourTextView: UIScrollView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var register_usr_pic: UIImageView!
    @IBOutlet weak var register_back_btn: UIButton!
    @IBOutlet weak var register_logo: UILabel!
    @IBOutlet weak var register_btn: UIButton!
    @IBOutlet weak var register_cpwd: UITextField!
    @IBOutlet weak var register_pwd: UITextField!
    @IBOutlet weak var register_email: UITextField!
    @IBOutlet weak var register_usr_name: UITextField!
    @IBOutlet weak var register_fname: UITextField!
    @IBOutlet weak var register_lname: UITextField!
    var image = UIImage()
    var imagePicker = UIImagePickerController()
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var activeField: UITextField?
        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     //   scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 2000)
            
            
            //MARK : - Circular Image
            register_usr_pic.layer.borderWidth = 1
            register_usr_pic.layer.masksToBounds = false
            register_usr_pic.layer.borderColor = UIColor.blackColor().CGColor
            register_usr_pic.layer.cornerRadius = register_usr_pic.frame.height/2
            register_usr_pic.clipsToBounds = true

            
            // MARK: - Loading Animation
            indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
            indicator.center = view.center
            view.addSubview(indicator)
            indicator.bringSubviewToFront(view)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            
            register_usr_pic.userInteractionEnabled = true
            let tapRecog = UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.imageTapped(_:)))
            register_usr_pic.addGestureRecognizer(tapRecog)
            let notificationCenter = NSNotificationCenter.defaultCenter()
            notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillHideNotification, object: nil)
            notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillChangeFrameNotification, object: nil)
       
       
        // MARK:- Setting textfield's background color
        register_fname.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        register_lname.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        register_email.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        register_usr_name.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        register_pwd.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        register_cpwd.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        
        
        //MARK: - Setting textfield's border color and border width
         register_fname.borderStyle = UITextBorderStyle.None
        register_lname.borderStyle = UITextBorderStyle.None
        register_usr_name.borderStyle = UITextBorderStyle.None
        register_email.borderStyle = UITextBorderStyle.None
        register_pwd.borderStyle = UITextBorderStyle.None
         register_cpwd.borderStyle = UITextBorderStyle.None
        
        let borderbot = CALayer()
        borderbot.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot.frame = CGRect(x: 0, y: register_cpwd.frame.height - 1, width: register_cpwd.frame.width, height: 1)
        borderbot.borderWidth = 1
        register_cpwd.layer.addSublayer(borderbot)
        register_cpwd.layer.masksToBounds = true
        
        let borderbot2 = CALayer()
        borderbot2.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot2.frame = CGRect(x: 0, y: register_pwd.frame.height - 1, width: register_pwd.frame.width, height: 1)
        borderbot2.borderWidth = 1
        register_pwd.layer.addSublayer(borderbot2)
        register_pwd.layer.masksToBounds = true
        
        let borderbot3 = CALayer()
        borderbot3.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot3.frame = CGRect(x: 0, y: register_email.frame.height - 1, width: register_email.frame.width, height: 1)
        borderbot3.borderWidth = 1
        register_email.layer.addSublayer(borderbot3)
        register_email.layer.masksToBounds = true
        
        let borderbot4 = CALayer()
        borderbot4.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot4.frame = CGRect(x: 0, y: register_usr_name.frame.height - 1, width: register_usr_name.frame.width, height: 1)
        borderbot4.borderWidth = 1
        register_usr_name.layer.addSublayer(borderbot4)
        register_usr_name.layer.masksToBounds = true
        
        let borderbot5 = CALayer()
        borderbot5.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot5.frame = CGRect(x: 0, y: register_lname.frame.height - 1, width: register_lname.frame.width, height: 1)
        borderbot5.borderWidth = 1
        register_lname.layer.addSublayer(borderbot5)
        register_lname.layer.masksToBounds = true
        
        let borderbot6 = CALayer()
        borderbot6.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot6.frame = CGRect(x: 0, y: register_fname.frame.height - 1, width: register_fname.frame.width, height: 1)
        borderbot6.borderWidth = 1
        register_fname.layer.addSublayer(borderbot6)
        register_fname.layer.masksToBounds = true
        
        let borderTop = CALayer()
        borderTop.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop.frame = CGRect(x: 0, y: 0, width: register_fname.frame.width, height: 1)
        borderTop.borderWidth = 1
        register_fname.layer.addSublayer(borderTop)
        register_fname.layer.masksToBounds = true
        
        // MARK: - register button title color
        register_btn.setTitleColor(UIColor(red: purple_color_red,green: purple_color_green,blue: purple_color_blue,alpha: 1), forState: .Normal)
        register_back_btn.setTitleColor((UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)), forState: .Normal)
        
        
        // MARK: - Logo color
        register_logo.textColor = UIColor(red: purple_color_red,green: purple_color_green,blue: purple_color_blue,alpha: 1)
        
        
        // MARK: - Register button function click cal
        register_btn.addTarget(self, action: #selector(self.register_user), forControlEvents: .TouchUpInside)
        
        
    }
    
    
    func register_user(sender: UIButton)
    {
//        let url:NSURL = NSURL(string: "https://thread-app.herokuapp.com/signup")!
//        let session = NSURLSession.sharedSession()
//        
//        let request = NSMutableURLRequest(URL: url)
//        request.HTTPMethod = "POST"
//        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
//        
//        
//        var data = ("email="+register_email.text!+"&password="+register_pwd.text!+"&username="+register_usr_name.text!)
//        
//        data = (data + "&fname="+register_fname.text!+"&lname="+register_lname.text!)
//        
//        let ns_data = data.dataUsingEncoding(NSUTF8StringEncoding)
//        
//        
//        let task = session.uploadTaskWithRequest(request, fromData: ns_data, completionHandler:
//            {(data,response,error) in
//                
//                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                    print(data)
//                    return
//                }
//                
//                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print(dataString)
//            }
//        );
//        
//        task.resume()
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) { [unowned self] in
//                    let url:NSURL = NSURL(string: "https://thread-app.herokuapp.com/signup")!
//                    let session = NSURLSession.sharedSession()
//            
//                    let request = NSMutableURLRequest(URL: url)
//                    request.HTTPMethod = "POST"
//                    request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
//            
//            
//                    var data = ("email="+self.register_email.text!+"&password="+self.register_pwd.text!+"&username="+self.register_usr_name.text!)
//            
//                    data = (data + "&fname="+self.register_fname.text!+"&lname="+self.register_lname.text!)
//            
//                    let ns_data = data.dataUsingEncoding(NSUTF8StringEncoding)
//            
//            
//                    let task = session.uploadTaskWithRequest(request, fromData: ns_data, completionHandler:
//                        {(data,response,error) in
//            
//                            guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                                print(data)
//                                
//                                return
//                            }
//                            
//                            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                           // print("My data- ",dataString["data"]!["_id"])
//                         //   usr_id = (dataString["data"]!["_id"])!
//                          
//                        }
//                    );
//                    
//                    task.resume()
            let url = NSURL(string:"https://thread-app.herokuapp.com/signup")
            let request = NSMutableURLRequest(URL: url!)
            request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let values = [ "email": self.register_email.text!,
                           "password": self.register_pwd.text!,
                           "username": self.register_usr_name.text!,
                            "fname": self.register_fname.text!,
                            "lname" : self.register_lname.text!,
                
            ]
            
            // request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            
            Alamofire.request(.POST, url!, parameters: values , encoding:.JSON).responseJSON
                { response in switch response.result {
                case .Success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    //  let bool_val = (response["status"]!)!
                    if(response["status"]! as! NSObject == 1)
                    {
                        //example if there is an id
                        let projId = (response["data"]!["_id"])!
                    //    self.tmp_proj_id = projId as! String
                      //  print("..........",self.tmp_proj_id)
                        //self.call_update_usr_tbl()
                        usr_id = projId!
                    }
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
                    self.call_to_upload_pic()
                    // completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
            }

        }
        
        
        
    }
    func adjustForKeyboard(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let keyboardViewEndFrame = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        
        if notification.name == UIKeyboardWillHideNotification {
            yourTextView.contentInset = UIEdgeInsetsZero
        } else {
            yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        yourTextView.scrollIndicatorInsets = yourTextView.contentInset
        
      //  let selectedRange = 100
       // yourTextView.scrollRangeToVisible(selectedRange)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imageTapped(gestureRecognizer: UITapGestureRecognizer)
    {
        var alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        var cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()
        }
        var gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallary()
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.navigationBar.tintColor = UIColor.blackColor()
            imagePicker.navigationItem.title = "Photo Albums"
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self .presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.navigationBar.tintColor = UIColor.blackColor()
        imagePicker.navigationItem.title = "Photo Albums"
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //PickerView Delegate Methods
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image1: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        
        image = image1
        register_usr_pic.image = image
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
    
    func call_to_upload_pic()
    {
        // MARK: - Uploading Image
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
        let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        let data = UIImageJPEGRepresentation(image, 0.5)
        data!.writeToURL(testFileURL1, atomically: true)
        uploadRequest1.bucket = "metaphor.thread"
        uploadRequest1.key =  usr_id
        uploadRequest1.body = testFileURL1
        
        let task1 = transferManager.upload(uploadRequest1)
        task1.continueWithBlock { (task1) -> AnyObject! in
            if task1.error != nil {
                print("Error: \(task1.error)")
            } else {
                print("Upload successful")
            }
            
            dispatch_async(dispatch_get_main_queue()) {
                        let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
                        let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
                     //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                
                        let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
                        readRequest1.bucket = "metaphor.thread"
                        readRequest1.key =  usr_id
                        readRequest1.downloadingFileURL = downloadingFileURL1
                
                        let task = transferManager.download(readRequest1)
                        task.continueWithBlock { (task) -> AnyObject! in
                            print(task.error)
                            if task.error != nil {
                            } else {
                                dispatch_async(dispatch_get_main_queue()
                                    , { () -> Void in
                                        usr_img = UIImage(contentsOfFile: downloadingFilePath1)!
                                        
                                })
                                print("Fetched image")
                            }
                            return nil
                        }
            }
            
            return nil
        }
    }

}
