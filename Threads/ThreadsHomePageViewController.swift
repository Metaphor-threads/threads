//
//  ThreadsHomePageViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 8/2/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class ThreadsHomePageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var threads_home_back_btn: UIButton!
    @IBOutlet weak var threads_home_add_btn: UIButton!
    @IBOutlet weak var threads_home_name_lbl : UILabel!
    @IBOutlet weak var threads_home_privacy_lbl : UILabel!
    @IBOutlet weak var threads_home_team_members : UILabel!
    @IBOutlet weak var threads_home_edit_btn : UIImageView!
    @IBOutlet weak var threads_home_proj_img : UIImageView!
    @IBOutlet weak var threads_home_no_of_posts : UILabel!
    @IBOutlet weak var threads_home_proj_name : UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 850; //Set this to any value that works for you.

        // MARK: - Setting Project's Details
        self.threads_home_set_details()
        
        // MARK: - Fetching Project Details
        self.threads_page_fetch_project_details()
        
        // MARK: - Fetching Users Details
        self.threads_home_fetch_user_details()
        
        // MARK: - Add new Project Button
        threads_home_add_btn.addTarget(self, action: #selector(add_new_post), forControlEvents: .TouchUpInside)
        
        // MARK: - Fetch Post details
    }
    
    func threads_page_fetch_project_details()
    {
         dispatch_async(dispatch_get_main_queue()) {
            
            
            let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
            let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
         
            
            let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
            readRequest1.bucket = "metaphor.thread"
            readRequest1.key =  selected_proj_id
            readRequest1.downloadingFileURL = downloadingFileURL1
            
            let task = transferManager.download(readRequest1)
            task.continueWithBlock { (task) -> AnyObject! in
                print(task.error)
                if task.error != nil {
                } else {
                    dispatch_async(dispatch_get_main_queue()
                        , { () -> Void in
                            self.threads_home_proj_img.image = UIImage(contentsOfFile: downloadingFilePath1)
                            
                    })
                    print("Fetched image")
                }
                return nil
            }
        }
    }
    
    func threads_home_set_details()
    {
        threads_home_proj_name.text = selected_proj_name
        threads_home_privacy_lbl.text = selected_proj_privacy
        
    }
    
    func threads_home_fetch_user_details()
    {
        let url = NSURL(string:"https://thread-app.herokuapp.com/userdetails")
        let parameters : [String:AnyObject] = [ "usr_id" : selected_proj_creator
        ]
        
        //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("ABLE TO FECTH USER DATA: \(JSON)")
                
                let response = JSON as! NSDictionary
                //  let bool_val = (response["status"]!)!
                if(response["status"]! as! NSObject == 1)
                {
                    let userId = response.objectForKey("usr_projs")
                    usr_proj_id_array = userId! as! NSArray
                    print("cmon-",usr_proj_id_array.count)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    
    func add_new_post()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("createPost") as! CreatePostViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
        
    }
    
    // MARK: - TableView Delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post_proj_details.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCellWithIdentifier("ThreadHomePage") as! ThreadHomePageCustomViewCell
        
        cell.post_story.text = post_proj_details[indexPath.row]["post_story"].string
        cell.layoutIfNeeded()
        
       //fetch image
        
        let cachedAvatar = postCache.imageWithIdentifier(post_proj_details[indexPath.row]["post_media"][0]["post_media_url"].string!)
        if(cachedAvatar == nil)
        {
            let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
            let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
            readRequest1.bucket = "metaphor.thread"
            print("my key",post_proj_details[indexPath.row]["post_media"][0]["post_media_url"].string!)
            readRequest1.key =  post_proj_details[indexPath.row]["post_media"][0]["post_media_url"].string!
            readRequest1.downloadingFileURL = downloadingFileURL1
            
            let task = transferManager.download(readRequest1)
            task.continueWithBlock { (task) -> AnyObject! in
                print(task.error)
                if task.error != nil {
                } else {
                    dispatch_async(dispatch_get_main_queue()
                        , { () -> Void in
                            cell.post_img1.image = UIImage(contentsOfFile: downloadingFilePath1)
                            cell.post_img1.setNeedsDisplay()
                            cell.post_img1.reloadInputViews()
                            
                    })
                    print("Fetched image")
                }
                return nil
            }
            
        }
        else
        {
            cell.post_img1.image = postCache.imageWithIdentifier(post_proj_details[indexPath.row]["post_media"][0]["post_media_url"].string!)
            cell.post_img1.setNeedsDisplay()
            cell.post_img1.reloadInputViews()
        }
        
        
        // fetching user's Image
        
        let usrImg = postCache.imageWithIdentifier(post_proj_details[indexPath.row]["post_usr_id"].string!)
        if(usrImg == nil)
        {
            let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
            let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
            readRequest1.bucket = "metaphor.thread"
            readRequest1.key =  post_proj_details[indexPath.row]["post_usr_id"].string!
            readRequest1.downloadingFileURL = downloadingFileURL1
            
            let task = transferManager.download(readRequest1)
            task.continueWithBlock { (task) -> AnyObject! in
                print(task.error)
                if task.error != nil {
                } else {
                    dispatch_async(dispatch_get_main_queue()
                        , { () -> Void in
                            cell.post_usr_img.image = UIImage(contentsOfFile: downloadingFilePath1)
                            cell.post_usr_img.setNeedsDisplay()
                            cell.post_usr_img.reloadInputViews()
                            
                    })
                    print("Fetched image")
                }
                return nil
            }
            
        }
        else
        {
            cell.post_usr_img.image = postCache.imageWithIdentifier(post_proj_details[indexPath.row]["post_usr_id"].string!)
            cell.post_usr_img.setNeedsDisplay()
            cell.post_usr_img.reloadInputViews()
        }
                
        return cell
    
    }
}
