//
//  ThreadCustomViewCell.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/21/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class ThreadCustomViewCell: UITableViewCell {
    @IBOutlet weak var proj_title: UILabel!
    @IBOutlet weak var proj_img: UIImageView!
    @IBOutlet weak var proj_team_members: UITextView!
     var threads_proj_id: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    /////////////////////////
    
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as! UICollectionViewCell
        if indexPath.row%2 == 0 {
            cell.backgroundColor = UIColor.redColor()
        } else {
            cell.backgroundColor = UIColor.yellowColor()
        }
        
        return cell
    }
    
}
