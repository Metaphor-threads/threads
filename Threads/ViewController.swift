//
//  ViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/12/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var start_logo: UILabel!
    @IBOutlet weak var start_login_btn: UIButton!
    @IBOutlet weak var start_register_btn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // MARK: - Logo Color
        start_logo.textColor = UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)
        
        // MARK: - Button's Color
        start_login_btn.layer.borderWidth = 1
        start_login_btn.layer.borderColor = UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1).CGColor
        start_login_btn.setTitleColor(UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1), forState: .Normal)
        
        start_register_btn.backgroundColor = UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)
        start_register_btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func  preferredStatusBarStyle()-> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }


}

