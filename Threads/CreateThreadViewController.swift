//
//  CreateThreadViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/22/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//


import UIKit
import Alamofire
import AWSS3



class CreateThreadViewController: UIViewController,UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var create_thread_proj_img: UIImageView!
    @IBOutlet weak var create_thread_description: UITextView!
    @IBOutlet weak var create_thread_post_btn: UIButton!
    @IBOutlet weak var create_thread_proj_switch: UISwitch!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var create_thread_proj_title: UITextField!
    var image = UIImage()
    var imagePicker = UIImagePickerController()
    var switch_state_text = "private"
    var tmp_proj_id = ""
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let dummy:NSData = NSData()
    override func viewDidLoad() {
       
        super.viewDidLoad()
      //  var image:UIImage = UIImage(named: "user_cam")!
        image = UIImage(named: "user_cam")!
        create_thread_proj_img.image = image
        open_usr_profile = false
        
        // MARK: - Loading Animation
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubviewToFront(view)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        
        create_thread_proj_img.userInteractionEnabled = true
        
        let tapRecog = UITapGestureRecognizer(target: self, action: #selector(CreateThreadViewController.imgTap(_:)))
        create_thread_proj_img.addGestureRecognizer(tapRecog)
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillChangeFrameNotification, object: nil)

        
     
        
        
        ////////////
        
//        let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
//        let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
//     //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//        
//        
//        let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
//        readRequest1.bucket = "metaphor.thread"
//        readRequest1.key =  "drone"
//        readRequest1.downloadingFileURL = downloadingFileURL1
//        
//        let task = transferManager.download(readRequest1)
//        task.continueWithBlock { (task) -> AnyObject! in
//            print(task.error)
//            if task.error != nil {
//            } else {
//                dispatch_async(dispatch_get_main_queue()
//                    , { () -> Void in
//                        self.create_thread_proj_img.image = UIImage(contentsOfFile: downloadingFilePath1)
//                        self.create_thread_proj_img.setNeedsDisplay()
//                        self.create_thread_proj_img.reloadInputViews()
//                        
//                })
//                print("Fetched image")
//            }
//            return nil
//        }
        
        ///////////
        
        
        
        
        // MARK: - Text View's PlaceHolder
        create_thread_description.text = "Project Description"
        create_thread_description.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        create_thread_description.delegate = self
        
        // MARK: - Post Button's Action
        create_thread_post_btn.addTarget(self, action: #selector(post_action), forControlEvents: .TouchUpInside)
        
        // MARK: - Setting background color
        create_thread_proj_title.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        create_thread_description.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        
        // MARK: - Button Color
        create_thread_post_btn.setTitleColor(UIColor(red: purple_color_red,green: purple_color_green,blue: purple_color_blue,alpha: 1), forState: .Normal)
        
        // MARK: - TextField's Border color
        let borderTop = CALayer()
        borderTop.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop.frame = CGRect(x: 0, y: 0, width: create_thread_proj_title.frame.width, height: 1)
        borderTop.borderWidth = 1
        self.create_thread_proj_title.layer.addSublayer(borderTop)
        self.create_thread_proj_title.layer.masksToBounds = true
        
        let borderTop1 = CALayer()
        borderTop1.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop1.frame = CGRect(x: 0, y: 0, width: create_thread_description.frame.width, height: 1)
        borderTop1.borderWidth = 1
        self.create_thread_description.layer.addSublayer(borderTop1)
        self.create_thread_description.layer.masksToBounds = true
        
        let borderBot = CALayer()
        borderBot.borderWidth = 1
        borderBot.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderBot.frame = CGRect(x: 0, y: create_thread_description.frame.height - 1, width: create_thread_description.frame.width, height: 1)
        self.create_thread_description.layer.addSublayer(borderBot)
        self.create_thread_description.layer.masksToBounds = true
        
        // MARK: - Switch
        create_thread_proj_switch.addTarget(self, action: #selector(switch_state), forControlEvents: .ValueChanged)
    }
    
    func switch_state(switchState: UISwitch)
    {
        if switchState.on {
            switch_state_text = "public"
        } else {
            switch_state_text = "private"
        }
    }
    
    func post_action(Sender : UIButton)
    {
        
      // MARK: - Starting Animation
        indicator.startAnimating()
        create_thread_post_btn.enabled = false
        create_thread_post_btn.alpha = 0.5
        
        
        
        // MARK: - Posting MongoDB data in Project's Table
        let url = NSURL(string:"https://thread-app.herokuapp.com/addproject")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = [ "proj_name": create_thread_proj_title.text! as String,
                               "proj_desc": create_thread_description.text,
                       "proj_team_members": [
                        
                            "proj_usr_id": usr_id
                        
            ],
                       "proj_privacy" : switch_state_text,
                       "proj_cover_img" : "google.com"
                    
        ]
        
       // request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        
        Alamofire.request(.POST, url!, parameters: values as! [String : AnyObject], encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("Success with JSON: \(JSON)")
                
                let response = JSON as! NSDictionary
                //  let bool_val = (response["status"]!)!
                if(response["status"]! as! NSObject == 1)
                {
                    //example if there is an id
                    let projId = (response["message"])!
                    self.tmp_proj_id = projId as! String
                    print("..........",self.tmp_proj_id)
                    self.call_update_usr_tbl()
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
                
               // completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
        }
        
        
//        Alamofire.request(request)
//            .responseJSON { response in
//                // do whatever you want here
//            
//                switch response.result {
//                case .Failure(let error):
//                    print(error)
//                case .Success(let responseObject):
//                    //self.tmp_proj_id = responseObject["success"]
//                    print("---",responseObject)
//                }
//        }
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        create_thread_description.text = ""
        create_thread_description.textColor = UIColor.blackColor()
        return true
    }
    
    func textViewDidChange(textView: UITextView) {
        
        if (create_thread_description.text.isEmpty) {
            create_thread_description.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
            create_thread_description.text = "Project Description"
            create_thread_description.resignFirstResponder()
        }
    }
    
    func imgTap(gestureRecognizer: UITapGestureRecognizer)
    {
        var alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        var cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()
        }
        var gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallary()
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.navigationBar.tintColor = UIColor.blackColor()
            imagePicker.navigationItem.title = "Photo Albums"
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self .presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.navigationBar.tintColor = UIColor.blackColor()
        imagePicker.navigationItem.title = "Photo Albums"
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //PickerView Delegate Methods
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image1: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        
        image = image1
        create_thread_proj_img.image = image
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
    
    
    func adjustForKeyboard(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let keyboardViewEndFrame = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        
        if notification.name == UIKeyboardWillHideNotification {
            scrollView.contentInset = UIEdgeInsetsZero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    
    func call_update_usr_tbl()
    {
        let url = NSURL(string:"https://thread-app.herokuapp.com/addprojectinuser")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let values = [ "proj_id": self.tmp_proj_id,
                       "usr_id": usr_id
            
        ]
        
        // request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        
        Alamofire.request(.PUT, url!, parameters: values, encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("Success with JSON: \(JSON)")
                
                let response = JSON as! NSDictionary
                //  let bool_val = (response["status"]!)!
//                if(response["status"]! as! NSObject == 1)
//                {
//                    //example if there is an id
//                   
//                }
                self.call_to_amazonS3(self.completionHandler)
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
                
                // completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
        }

    }
    
    func call_to_amazonS3(completion: (message: String?) -> Void)
    {
        // MARK: - Uploading Image
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
        let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        let data = UIImageJPEGRepresentation(image, 0.5)
        data!.writeToURL(testFileURL1, atomically: true)
        uploadRequest1.bucket = "metaphor.thread"
        uploadRequest1.key =  self.tmp_proj_id
        uploadRequest1.body = testFileURL1
        
        let task1 = transferManager.upload(uploadRequest1)
        task1.continueWithBlock { (task1) -> AnyObject! in
            if task1.error != nil {
                print("Error: \(task1.error)")
            } else {
                print("Upload successful")
            }
             load_thread()
            completion(message: NSString(data: self.dummy, encoding: NSUTF8StringEncoding) as? String)
            return nil
        }
    }
    
    func completionHandler(message: String?){
        // MARK: - Stop Animation
        indicator.startAnimating()
        create_thread_post_btn.enabled = true
        create_thread_post_btn.alpha = 1
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            
            
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("myTabBar") as! MyTabBarController
            NSOperationQueue.mainQueue().addOperationWithBlock {
                self.presentViewController(nextViewController, animated:true, completion:nil)
            
        }
    }
    

}
