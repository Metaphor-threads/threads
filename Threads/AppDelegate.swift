//
//  AppDelegate.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/12/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import AWSCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let cognitoAccountId = "xxxxxxxxxxxxxxxxx"
    let cognitoIdentityPoolId = "us-east-1:4e92bd67-5cfc-43e3-96d6-3b37fb9c9c31"
    let cognitoUnauthRoleArn = "xxxxxxxxxxxxxxxxx"
    let cognitoAuthRoleArn = "xxxxxxxxxxxxxxxxx"


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
       
//        AWSCognitoCredentialsProvider.initialize()
//        
//        var credentialsProvider = AWSCognitoCredentialsProvider(
//            regionType: AWSRegionType.USEast1,
//            identityPoolId: "us-east-1:c9861ce9-beb9-4128-8b82-30a9d1494963"
//        )
//        
//        var configuration = AWSServiceConfiguration(
//            region: AWSRegionType.USEast1,
//            credentialsProvider: credentialsProvider
//        )
//        
//        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration

        AWSCognitoCredentialsProvider.initialize()
        
        
        
        var credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.USEast1,
            identityPoolId: cognitoIdentityPoolId
        )
        
        let configuration = AWSServiceConfiguration(
            region: AWSRegionType.USEast1,
            credentialsProvider: credentialsProvider)
       AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration

        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

