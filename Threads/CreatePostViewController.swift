//
//  CreatePostViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 8/2/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class CreatePostViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var yourTextView: UIScrollView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var create_post_thread_name : UILabel!
    @IBOutlet weak var create_post_story : UITextView!
    @IBOutlet weak var create_post_tags : UITextField!
    @IBOutlet weak var create_post_img1 : UIImageView!
    @IBOutlet weak var create_post_add_new_post : UIButton!
    var imagePicker = UIImagePickerController()
    var  image = UIImage()
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    var randomString : NSMutableString = NSMutableString(capacity: 32)
    var post_string = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // MARK: - Keyboard Scroll
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillChangeFrameNotification, object: nil)
        
        // MARK: - Setting Thread Name
        create_post_thread_name.text = selected_proj_name
        
        // MARK: - Story's PlaceHolder
        create_post_story.text = "Project Description"
        create_post_story.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        create_post_story.delegate = self
        
        // MARK: - Image Tapping
        create_post_img1.userInteractionEnabled = true
        
        let tapRecog = UITapGestureRecognizer(target: self, action: #selector(CreatePostViewController.imgTap(_:)))
        create_post_img1.addGestureRecognizer(tapRecog)
        
        // MARK: - Add new Post button
        create_post_add_new_post.addTarget(self, action: #selector(add_new_post_call), forControlEvents: .TouchUpInside)

    }
    
    func update_mongodb()
    {
        let url = NSURL(string:"https://thread-app.herokuapp.com/addpost")
        let parameters : [String:AnyObject] = [ "post_usr_id" : usr_id,
                                                "proj_id" : selected_proj_id,
                                                "post_story" : create_post_story.text,
                                                "post_media" : [
                                                        "post_media_url" : post_string
            ]
        ]
        
        //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        print("are we here-",parameters)
        
        Alamofire.request(.PUT, url!, parameters: parameters, encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("ABLE TO FECTH USER DATA: \(JSON)")
                
                let response = JSON as! NSDictionary
                //  let bool_val = (response["status"]!)!
                if(response["status"]! as! NSObject == 1)
                {
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    func add_new_post_call()
    {
       
        
        for var i=0; i < 32; i++ {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
     
        post_string = "1" + usr_id+"_"+selected_proj_id+"_"+(randomString as String)
       print(post_string)
        self.update_mongodb()
        // MARK: - Uploading Image
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
        let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        let data = UIImageJPEGRepresentation(image, 0.5)
        data!.writeToURL(testFileURL1, atomically: true)
        uploadRequest1.bucket = "metaphor.thread"
        uploadRequest1.key =  post_string as String
        uploadRequest1.body = testFileURL1
        
        let task1 = transferManager.upload(uploadRequest1)
        task1.continueWithBlock { (task1) -> AnyObject! in
            if task1.error != nil {
                print("Error: \(task1.error)")
            } else {
                print("Upload successful")
            }
            return nil
        }
        
    }
    
    func adjustForKeyboard(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let keyboardViewEndFrame = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        
        if notification.name == UIKeyboardWillHideNotification {
            yourTextView.contentInset = UIEdgeInsetsZero
        } else {
            yourTextView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        yourTextView.scrollIndicatorInsets = yourTextView.contentInset
        
        //  let selectedRange = 100
        // yourTextView.scrollRangeToVisible(selectedRange)
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        create_post_story.text = ""
        create_post_story.textColor = UIColor.blackColor()
        return true
    }
    
    func textViewDidChange(textView: UITextView) {
        
        if (create_post_story.text.isEmpty) {
            create_post_story.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
            create_post_story.text = "Project Description"
            create_post_story.resignFirstResponder()
        }
    }
    
    // MARK: - Image Picker
    func imgTap(gestureRecognizer: UITapGestureRecognizer)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.navigationBar.tintColor = UIColor.blackColor()
            imagePicker.navigationItem.title = "Photo Albums"
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self .presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        imagePicker.navigationBar.tintColor = UIColor.blackColor()
        imagePicker.navigationItem.title = "Photo Albums"
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //PickerView Delegate Methods
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image1: UIImage!, editingInfo: NSDictionary!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        
        image = image1
        create_post_img1.image = image
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        print("picker cancel.")
    }
    
    
}
