//
//  PostTabViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 8/2/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class PostTabViewController: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("createPost") as! CreatePostViewController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
}
