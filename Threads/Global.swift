//
//  Global.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/15/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

let photoCache = AutoPurgingImageCache(
    memoryCapacity: 100 * 1024 * 1024,
    preferredMemoryUsageAfterPurge: 60 * 1024 * 1024
)

// Post Details Cahce
let postCache = AutoPurgingImageCache(
    memoryCapacity: 100 * 1024 * 1024,
    preferredMemoryUsageAfterPurge: 60 * 1024 * 1024
)

// MARK: - User Local Data
struct defaultsKeys {
    static let local_user_id = ""
    static let keyTwo = "secondStringKey"
}


let dummy_text:NSData = NSData()

let textfield_bg_color_red:CGFloat  = 250/255
let textfield_bg_color_green:CGFloat = 250/255
let textfield_bg_color_blue:CGFloat = 250/255

let textfield_placeholder_red: CGFloat = 151/255
let textfield_placeholder_green:CGFloat = 151/255
let textfield_placeholder_blue: CGFloat = 151/255

// Mark: - Register Page Border Width
let register_border_width:CGFloat = 1

// MARK: - Purple color codes
let purple_color_red : CGFloat = 154/255
let purple_color_green: CGFloat = 0/255
let purple_color_blue: CGFloat = 187/255


// MARK: - Screen Size
let screenSize: CGRect = UIScreen.mainScreen().bounds
let width = screenSize.width
let height = screenSize.height

// MARK: - Tab bar Flags
var open_usr_profile:Bool = false


// MARK: - User Details
var usr_id:String = ""
var usr_fname:String = ""
var usr_lname:String = ""
var usr_img:UIImage = UIImage()
var usr_proj_id_array = []
var usr_proj_title_array:[String:String] = [:]

// MARK: - Project Details
var selected_proj_id = ""
var selected_proj_name = ""
var selected_proj_privacy = ""
var selected_proj_posts = []
var selected_proj_creator = ""


// MARK: - Post Details
var post_proj_details = [JSON]()
var post_media_urls = [JSON]()
var post_user_created = [JSON]()


// MARK: - Load Threads Function
func load_thread() {
    // MARK: - Fetching Project's Details
    let url = NSURL(string:"https://thread-app.herokuapp.com/userdetails")
    let parameters : [String:AnyObject] = [ "usr_id" : usr_id
    ]
    
    //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
    
    
    Alamofire.request(.GET, url!, parameters: parameters).responseJSON
        { response in switch response.result {
        case .Success(let JSON):
            print("ABLE TO FECTH USER DATA: \(JSON)")
            
            let response = JSON as! NSDictionary
            //  let bool_val = (response["status"]!)!
            if(response["status"]! as! NSObject == 1)
            {
                //example if there is an id
                let userId = response.objectForKey("usr_projs")
                usr_proj_id_array = userId! as! NSArray
                print("cmon-",usr_proj_id_array.count)
                //   self.usr_proj_id_array = response["message"]!["usr_projs"] as! NSDictionary
                // print("usr projects-",self.usr_proj_id_array.count)
            }
            
            
            
        case .Failure(let error):
            print("Request failed with error: \(error)")
            }
    }
}