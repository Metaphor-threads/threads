//
//  ThreadViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/21/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3
import AlamofireImage

class ThreadViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
   
   @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print("Inside-",usr_proj_id_array.count)
        if(usr_proj_id_array.count > 0)
        {
            return usr_proj_id_array.count
        }
        return 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
      //  let cell = tableView.dequeueReusableCellWithIdentifier("ThreadPage", forIndexPath: indexPath)
        let cell = tableView.dequeueReusableCellWithIdentifier("ThreadPage") as! ThreadCustomViewCell
         cell.proj_img.image = nil
        cell.proj_title!.text = nil
         cell.tag = indexPath.row
        // Configure the cell...
        // cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        //print(".........",projects[indexPath.row]["project_name"])
        //cell.textLabel?.text = projects[indexPath.row]["project_name"] as? String
       // print("my label text----",usr_proj_id_array[indexPath.row]["usr_projs"])
        if(usr_proj_id_array.count > 0 && indexPath.row > 0)
        {
            
            let url = NSURL(string:"https://thread-app.herokuapp.com/projectdetails")
            let request = NSMutableURLRequest(URL: url!)
            //  request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let parameters : [String:AnyObject] = [ "proj_id" : usr_proj_id_array[indexPath.row]["proj_id"]
            ]
            
            //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            
            Alamofire.request(.GET, url!, parameters: parameters).responseJSON
                { response in switch response.result {
                case .Success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    //  let bool_val = (response["status"]!)!
                    if(response["status"]! as! NSObject == 1)
                    {
                        //example if there is an id
                      //  let userId = response.objectForKey("usr_projs")
                        print(" ",indexPath.row,"here",response["message"]!["proj_name"])
                        cell.proj_title!.text = response["message"]!["proj_name"]
                                             //  cell.proj_team_members!.text = "Hello Again"
                        let proj_id_tmp = response["message"]!["_id"]
                        usr_proj_title_array[proj_id_tmp!!] = cell.proj_title!.text
                        
                        print("Project count-",proj_id_tmp!)
                        let cachedAvatar = photoCache.imageWithIdentifier(proj_id_tmp!!)
                        print("Before Adding",cachedAvatar)
                        if(cachedAvatar == nil)
                        {
                            
                            let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
                            let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
                            //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                            
                            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                            let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
                            let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                            
                            let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
                            readRequest1.bucket = "metaphor.thread"
                            readRequest1.key =  response["message"]!["_id"]
                            readRequest1.downloadingFileURL = downloadingFileURL1
                            
                            let task = transferManager.download(readRequest1)
                            task.continueWithBlock { (task) -> AnyObject! in
                                print(task.error)
                                if task.error != nil {
                                } else {
                                    dispatch_async(dispatch_get_main_queue()
                                        , { () -> Void in
                                            cell.proj_img.image = UIImage(contentsOfFile: downloadingFilePath1)
                                            cell.proj_img.setNeedsDisplay()
                                            cell.proj_img.reloadInputViews()
                                            photoCache.addImage(cell.proj_img.image!, withIdentifier: proj_id_tmp!!)
                                            print("Added to Cache---",proj_id_tmp)
                                             
                                            
                                    })
                                    print("Fetched image")
                                }
                                return nil
                            }
                        }
                        else
                        {
                            cell.proj_img.image = photoCache.imageWithIdentifier(proj_id_tmp!!)
                            cell.proj_img.setNeedsDisplay()
                            cell.proj_img.reloadInputViews()
                            print("from cache")
                        }
                        
//                        else
//                        {
//                            print("Fetched from cache")
//                            let cachedAvatar = photoCache.imageWithIdentifier(proj_id_tmp!!)
//                            cell.proj_img.image = cachedAvatar
//                            cell.proj_img.setNeedsDisplay()
//                            cell.proj_img.reloadInputViews()
//                        }
                        //////
//                    
//                                let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
//                                let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
//                             //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//                        
//                                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//                                let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
//                                let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
//                        
//                                let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
//                                readRequest1.bucket = "metaphor.thread"
//                                readRequest1.key =  response["message"]!["_id"]
//                                readRequest1.downloadingFileURL = downloadingFileURL1
//                        
//                                let task = transferManager.download(readRequest1)
//                                task.continueWithBlock { (task) -> AnyObject! in
//                                    print(task.error)
//                                    if task.error != nil {
//                                    } else {
//                                        dispatch_async(dispatch_get_main_queue()
//                                            , { () -> Void in
//                                                cell.proj_img.image = UIImage(contentsOfFile: downloadingFilePath1)
//                                                cell.proj_img.setNeedsDisplay()
//                                                cell.proj_img.reloadInputViews()
//                                                photoCache.addImage(cell.proj_img.image!, withIdentifier: proj_id_tmp!!)
//                                                print("Added to Cache")
//                                        })
//                                        print("Fetched image")
//                                    }
//                                    return nil
//                                }
                        //////
                    }
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
                    
                    //  completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
            }
          
            
            
            
            
            
            print("my label text----",usr_proj_id_array[0]["proj_id"])
           
        }
        else
        {
            let url = NSURL(string:"https://thread-app.herokuapp.com/projectdetails")
            let request = NSMutableURLRequest(URL: url!)
            //  request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let parameters : [String:AnyObject] = [ "proj_id" : usr_proj_id_array[0]["proj_id"]
            ]
            
            //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
           
            
            Alamofire.request(.GET, url!, parameters: parameters).responseJSON
                { response in switch response.result {
                case .Success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    //  let bool_val = (response["status"]!)!
                    if(response["status"]! as! NSObject == 1)
                    {
                        //example if there is an id
                        //  let userId = response.objectForKey("usr_projs")
                        print(" ",indexPath.row,"here",response["message"]!["proj_name"])
                        cell.proj_title!.text = response["message"]!["proj_name"]
                       // cell.proj_team_members.text = "Hello Again"
                      
                        
                        let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
                        let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
                        //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        let testFileURL1 = NSURL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingString("temp"))
                        let uploadRequest1 : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
                        readRequest1.bucket = "metaphor.thread"
                        readRequest1.key =  "my_gallery_pic"
                        readRequest1.downloadingFileURL = downloadingFileURL1
                        
                        let task = transferManager.download(readRequest1)
                        task.continueWithBlock { (task) -> AnyObject! in
                            print(task.error)
                            if task.error != nil {
                            } else {
                                dispatch_async(dispatch_get_main_queue()
                                    , { () -> Void in
                                        cell.proj_img.image = UIImage(contentsOfFile: downloadingFilePath1)
                                        cell.proj_img.setNeedsDisplay()
                                        cell.proj_img.reloadInputViews()
                                        
                                })
                                print("Fetched image")
                            }
                            return nil
                        }

                    }
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
                    
                    //  completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        print("selected cell",indexPath.row)
        let customCell: ThreadCustomViewCell = (tableView.cellForRowAtIndexPath(indexPath) as! ThreadCustomViewCell)
       // let labelText: String = customCell.proj_title.text!
       // let threadId: String = customCell.threads_proj_id.text!
        selected_proj_id = (usr_proj_id_array[indexPath.row]["proj_id"])!!
        print("threads_proj_id",selected_proj_id)
        
        dispatch_async(dispatch_get_main_queue()) {
            let url = NSURL(string:"https://thread-app.herokuapp.com/projectdetails")
            let request = NSMutableURLRequest(URL: url!)
            //  request.HTTPMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let parameters : [String:AnyObject] = [ "proj_id" : selected_proj_id
            ]
            
            //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
            
            
            Alamofire.request(.GET, url!, parameters: parameters).responseJSON
                { response in switch response.result {
                case .Success(let JSON):
                    print("Success with JSON: \(JSON)")
                    
                    let response = JSON as! NSDictionary
                    //  let bool_val = (response["status"]!)!
                    if(response["status"]! as! NSObject == 1)
                    {
                        //example if there is an id
                        selected_proj_name = (response["message"]!["proj_name"])!!
                        selected_proj_privacy = (response["message"]!["proj_privacy"])!!
                        selected_proj_creator = (response["message"]!["proj_creator"])!!
                        
                       Helper.helper.fetch_post_details()
                    }
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
                    // completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
            }
            
        }
        
        
        

    }
   
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRectMake(0, 0, width, 200)) //set these values as necessary
        returnedView.backgroundColor = UIColor.blackColor()
        
        let label = UILabel(frame: CGRectMake(self.view.frame.width/2, 0, 200, height/16))
        label.text = "Thread"
       // label.textAlignment = .Center
        label.textColor = .whiteColor()
        returnedView.addSubview(label)
        
        let imageViewGame = UIImageView(frame: CGRectMake(width - 2*width/16, 2.5, 2*width/16, 0.9*height/16));
        //var image = UIImage(named: "Games.png");
        let image = UIImage(data: NSData(contentsOfURL: NSURL(string:"https://media-cdn.tripadvisor.com/media/photo-s/03/9b/2d/f2/new-york-city.jpg")!)!)
        imageViewGame.image = image;
        returnedView.addSubview(imageViewGame)
        
        // MARK: - Making image circle
        imageViewGame.layer.borderWidth = 1
        imageViewGame.layer.masksToBounds = false
        imageViewGame.layer.borderColor = UIColor.blackColor().CGColor
        imageViewGame.layer.cornerRadius = imageViewGame.frame.height/2
        imageViewGame.clipsToBounds = true
        
        imageViewGame.userInteractionEnabled = true
        
        let tapRecog = UITapGestureRecognizer(target: self, action: "imgTap:")
        imageViewGame.addGestureRecognizer(tapRecog)
        
        return returnedView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return height/13
    }
    
    
    func imgTap(gestureRecognizer: UITapGestureRecognizer)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("create_thread") as! CreateThreadViewController
        
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    
    func imageWithImage(image:UIImage,scaledToSize newSize:CGSize)->UIImage{
        
        UIGraphicsBeginImageContext( newSize )
        image.drawInRect(CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage.imageWithRenderingMode(.AlwaysTemplate)
    }
    
    override func  preferredStatusBarStyle()-> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }


}
