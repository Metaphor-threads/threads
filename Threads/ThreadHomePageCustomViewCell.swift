//
//  ThreadHomePageCustomViewCell.swift
//  Threads
//
//  Created by Pranav Bhandari on 8/4/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class ThreadHomePageCustomViewCell: UITableViewCell {
    @IBOutlet weak var post_creator: UILabel!
    @IBOutlet weak var post_usr_img: UIImageView!
    @IBOutlet weak var post_story: UITextView!
    @IBOutlet weak var post_img1 : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        post_usr_img.layer.borderWidth = 1
        post_usr_img.layer.masksToBounds = true
        post_usr_img.layer.borderColor = UIColor.blackColor().CGColor
        post_usr_img.layer.cornerRadius = post_usr_img.frame.width/2
        post_usr_img.clipsToBounds = true

    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

