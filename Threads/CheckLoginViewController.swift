//
//  CheckLoginViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/21/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class CheckLoginViewController: UIViewController, HolderViewDelegate {
    
    var holderView = HolderView(frame: CGRectZero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        addHolderView()
        let defaults = NSUserDefaults.standardUserDefaults()
        let stringOne = defaults.stringForKey(defaultsKeys.local_user_id)
        print("user defaults-",stringOne)
        if(stringOne != nil)
        {
            usr_id = stringOne!
            dispatch_async(dispatch_get_main_queue()) {
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
                let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
                //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                
                let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
                readRequest1.bucket = "metaphor.thread"
                readRequest1.key =  usr_id
                readRequest1.downloadingFileURL = downloadingFileURL1
                
                let task = transferManager.download(readRequest1)
                task.continueWithBlock { (task) -> AnyObject! in
                    print(task.error)
                    if task.error != nil {
                    } else {
                        dispatch_async(dispatch_get_main_queue()
                            , { () -> Void in
                                usr_img = UIImage(contentsOfFile: downloadingFilePath1)!
                                
                        })
                        print("Fetched image")
                    }
                    return nil
                }
            }
            
        //    self.load_threadsPage(completionHandler)
            
            
        }
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("StartPage") as! ViewController
            self.presentViewController(nextViewController, animated:true, completion:nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func addHolderView() {
        let boxSize: CGFloat = 100.0
        holderView.frame = CGRect(x: view.bounds.width / 2 - boxSize / 2,
                                  y: view.bounds.height / 2 - boxSize / 2,
                                  width: boxSize,
                                  height: boxSize)
        holderView.parentFrame = view.frame
        holderView.delegate = self
        view.addSubview(holderView)
        holderView.addOval()
    }
    
    func animateLabel() {
        // 1
        holderView.removeFromSuperview()
        view.backgroundColor = Colors.blue
        
        // 2
        let label: UILabel = UILabel(frame: view.frame)
        label.textColor = Colors.white
        label.font = UIFont(name: "HelveticaNeue-Thin", size: 100.0)
        label.textAlignment = NSTextAlignment.Center
        label.text = "Thread"
        label.transform = CGAffineTransformScale(label.transform, 0.25, 0.25)
        view.addSubview(label)
        
        // 3
        UIView.animateWithDuration(0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.CurveEaseInOut,
                                   animations: ({
                                    label.transform = CGAffineTransformScale(label.transform, 4.0, 4.0)
                                   }), completion: { finished in
                                    self.addButton()
                                   // self.load_threadsPage(self.completionHandler)
                                    self.get_project_data(self.completionHandler)
        })
    }
    
    func addButton() {
        let button = UIButton()
        button.frame = CGRectMake(0.0, 0.0, view.bounds.width, view.bounds.height)
        button.addTarget(self, action: "buttonPressed:", forControlEvents: .TouchUpInside)
        view.addSubview(button)
    }
    
    func buttonPressed(sender: UIButton!) {
        view.backgroundColor = Colors.white
        view.subviews.map({ $0.removeFromSuperview() })
        holderView = HolderView(frame: CGRectZero)
        addHolderView()
    }
    func load_threadsPage(completion: (message: String?) -> Void) {
        dispatch_async(dispatch_get_main_queue()) {
            load_thread()
            completion(message: NSString(data: dummy_text, encoding: NSUTF8StringEncoding) as? String)
        }
    }
    
    func completionHandler(message: String?){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("myTabBar") as! MyTabBarController
        self.presentViewController(nextViewController, animated:true, completion:nil)
    }
    
    func get_project_data(completion: (message: String?) -> Void) {
        let url = NSURL(string:"https://thread-app.herokuapp.com/userdetails")
        let parameters : [String:AnyObject] = [ "usr_id" : usr_id
        ]
        
        //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        
        Alamofire.request(.GET, url!, parameters: parameters).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("ABLE TO FECTH USER DATA: \(JSON)")
                
                let response = JSON as! NSDictionary
                //  let bool_val = (response["status"]!)!
                if(response["status"]! as! NSObject == 1)
                {
                   let userId = response.objectForKey("usr_projs")
                    usr_proj_id_array = userId! as! NSArray
                    print("cmon-",usr_proj_id_array.count)
                }
                completion(message: NSString(data: dummy_text, encoding: NSUTF8StringEncoding) as? String)
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }

}


