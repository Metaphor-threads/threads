//
//  ProfileViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/18/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    let myImages = ["pwd_icon.png","pwd_icon.png","pwd_icon.png"]
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var profile_usr_img : UIImageView!
    @IBOutlet weak var profile_usr_name : UILabel!
    @IBOutlet weak var profile_usr_username : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Project Arays-",photoCache.imageWithIdentifier("579be20c51dd8c0300137991"))
        
        
        // MARK: - Setting Usr Profile Pic
        profile_usr_img.image = usr_img

        
    }
    
    
    override func viewDidAppear(animated: Bool) {
       // tabBar.selectedItem = tabBar.items![4]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInCollectionView(collectionView:UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView:UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return usr_proj_id_array.count
    }
    
    func collectionView(collectionView:UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("profile_projects", forIndexPath: indexPath) as! ProjectsCustomViewCell
        
        //  let tmp_img :UIImage = UIImage(named: myImages[indexPath.row])!
        let tmp_prj_id = (usr_proj_id_array[indexPath.row]["proj_id"])!
        print("current array",tmp_prj_id!,"---",usr_proj_title_array[tmp_prj_id!])
       cell.profile_projects_img.image = photoCache.imageWithIdentifier(tmp_prj_id!)
        cell.profile_projects_lbl!.text = usr_proj_title_array[tmp_prj_id!]
   //     cell.profile_projects_img.setNeedsDisplay()
     //   cell.profile_projects_img.reloadInputViews()
        //cell.profile_projects_img.image = UIImage(named: "user_cam")
        // cell.projects_titleLabel?.text = "my project"
        
        return cell
    }

    
    
}