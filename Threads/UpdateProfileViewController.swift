//
//  UpdateProfileViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/19/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import AlamofireImage

class UpdateProfileViewController : UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    @IBOutlet weak var logout: UIButton!
    @IBOutlet weak var update_profile_lname: UITextField!
    @IBOutlet weak var update_profile_fname: UITextField!
    @IBOutlet weak var update_profile_email: UITextField!
    @IBOutlet weak var update_profile_pic: UIImageView!
    @IBOutlet weak var update_profile_username: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        // MARK: - Removing borders
        update_profile_email.borderStyle = UITextBorderStyle.None
        update_profile_fname.borderStyle = UITextBorderStyle.None
        update_profile_username.borderStyle = UITextBorderStyle.None
        update_profile_lname.borderStyle = UITextBorderStyle.None
        
        // MARK: - Textfield Background color
        update_profile_email.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        update_profile_username.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        update_profile_fname.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        update_profile_lname.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        
        // MARK: - Textfield text color
        update_profile_email.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        update_profile_username.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        update_profile_lname.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        update_profile_fname.textColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1)
        
        // MARK: - Circular Image
        update_profile_pic.layer.borderWidth = 1
        update_profile_pic.layer.masksToBounds = false
        update_profile_pic.layer.borderColor = UIColor.blackColor().CGColor
        update_profile_pic.layer.cornerRadius = update_profile_pic.frame.height/2
        update_profile_pic.clipsToBounds = true
        let cachedAvatar = photoCache.imageWithIdentifier("user_profile")
        print("Before Adding",cachedAvatar)
        if(cachedAvatar == nil)
        {
            photoCache.addImage(usr_img, withIdentifier: "user_profile")
            print("Added to Cache")
        }
        
        update_profile_pic.image = usr_img
        
        
        // MARK: - Textfield's border
        let borderbot = CALayer()
        borderbot.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot.frame = CGRect(x: 0, y: 0, width: update_profile_username.frame.width, height: 1)
        borderbot.borderWidth = 1
        update_profile_username.layer.addSublayer(borderbot)
        update_profile_username.layer.masksToBounds = true
        
        
        
        let borderbot1 = CALayer()
        borderbot1.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderbot1.frame = CGRect(x: 0, y: 0, width: update_profile_username.frame.width, height: 1)
        borderbot1.borderWidth = 1
        update_profile_username.layer.addSublayer(borderbot1)
        update_profile_username.layer.masksToBounds = true
        
        let borderTop2 = CALayer()
        borderTop2.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop2.frame = CGRect(x: 0, y: 0, width: update_profile_fname.frame.width, height: 1)
        borderTop2.borderWidth = 1
        update_profile_fname.layer.addSublayer(borderTop2)
        update_profile_fname.layer.masksToBounds = true
        
        let borderTop3 = CALayer()
        borderTop3.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop3.frame = CGRect(x: 0, y: 0, width: update_profile_lname.frame.width, height: 1)
        borderTop3.borderWidth = 1
        update_profile_lname.layer.addSublayer(borderTop3)
        update_profile_lname.layer.masksToBounds = true
        
        let borderTop4 = CALayer()
        borderTop4.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop4.frame = CGRect(x: 0, y: 0, width: update_profile_email.frame.width, height: 1)
        borderTop4.borderWidth = 1
        update_profile_email.layer.addSublayer(borderTop4)
        update_profile_email.layer.masksToBounds = true
        
        let borderBot2 = CALayer()
        borderBot2.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderBot2.frame = CGRect(x: 0, y: update_profile_email.frame.height, width: update_profile_email.frame.width, height: 1)
        borderBot2.borderWidth = 1
        update_profile_email.layer.addSublayer(borderBot2)
        update_profile_email.layer.masksToBounds = true
        
        logout.addTarget(self, action: #selector(self.log_out), forControlEvents: .TouchUpInside)
        
    }
    func log_out(sender : UIButton) {
        let picker=UIImagePickerController()
//        picker.delegate = self
//        picker.allowsEditing = false
//        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//        presentViewController(picker, animated: true, completion: nil)
        
     //   picker = UIImagePickerController()
        picker.navigationBar.tintColor = UIColor.blackColor()
        picker.navigationItem.title = "Photo Albums"
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: [NSObject: AnyObject]!){
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
     //   print("my image",image)
       // update_profile_pic.image = image
        
        
        
        /////////
        self.update_profile_pic.image = image
        self.update_profile_pic.layer.borderColor = UIColor.whiteColor().CGColor
        self.update_profile_pic.layer.borderWidth = 1.0
       

        ////////
        
    }
    
    
   
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
}
