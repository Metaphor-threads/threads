//
//  LoginViewController.swift
//  Threads
//
//  Created by Pranav Bhandari on 7/15/16.
//  Copyright © 2016 Pranav Bhandari. All rights reserved.
//

import UIKit
import Alamofire
import AWSS3

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    
   
    @IBOutlet weak var login_username: UITextField!
    
    
    
    
    @IBOutlet weak var login_signIn: UIButton!
    
    @IBOutlet weak var login_back_btn: UIButton!
    
    
    @IBOutlet weak var login_password: UITextField!
    @IBOutlet weak var login_frgtPwd: UIButton!

    @IBOutlet weak var Login_ScrollView: UIScrollView!
    
    @IBOutlet weak var login_logo: UILabel!
    
    @IBOutlet weak var Login_yourtextview: UIScrollView!
   

        
    
    
   
       
        
        
       

        
       
    
   
    
    var allow_login = ""
    let dummy = NSData()
    var indicator: UIActivityIndicatorView = UIActivityIndicatorView()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIKeyboardWillChangeFrameNotification, object: nil)
        
        
        login_username.layer.masksToBounds = true
        login_username.layer.borderColor = UIColor( red: 250/255, green: 250/255, blue:250/255, alpha: 3.0 ).CGColor
        login_username.layer.borderWidth = 2.0
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        // MARK: - Loading Animation
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        indicator.center = view.center
        view.addSubview(indicator)
        indicator.bringSubviewToFront(view)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true

        
        // MARK: - Icons
     /*   let vwContainer: UIView = UIView()
        vwContainer.frame = CGRectMake(0.0, 0.0, 50.0, 45.0)
        vwContainer.backgroundColor = UIColor.clearColor()
        let icon: UIImageView = UIImageView()
        icon.image = UIImage(named: "usr_icon.png")!
        icon.frame = CGRectMake(0.0, 0.0, 40.0, 40.0)
        icon.backgroundColor = UIColor.lightGrayColor()
        vwContainer.addSubview(icon)
        self.login_username.leftView = vwContainer
        self.login_username.leftViewMode = .Always
        
        let vwContainer1: UIView = UIView()
        vwContainer1.frame = CGRectMake(0.0, 0.0, 50.0, 45.0)
        vwContainer1.backgroundColor = UIColor.clearColor()
        let icon1: UIImageView = UIImageView()
        icon1.image = UIImage(named: "pwd_icon.png")!
        icon1.frame = CGRectMake(0.0, 0.0, 35.0, 35.0)
        icon1.backgroundColor = UIColor.lightGrayColor()
        vwContainer1.addSubview(icon1)
        self.login_password.leftView = vwContainer1
        self.login_password.leftViewMode = .Always
        
        // MARK: - Logo's Color
        login_logo.textColor = UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)
        
        // MARK: - Textfield's Border
        login_username.borderStyle = UITextBorderStyle.None
        login_password.borderStyle = UITextBorderStyle.None
        
        let borderTop = CALayer()
        borderTop.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop.frame = CGRect(x: 0, y: 0, width: login_username.frame.width, height: 1)
        borderTop.borderWidth = 1
        self.login_username.layer.addSublayer(borderTop)
        self.login_username.layer.masksToBounds = true
        
        let borderTop2 = CALayer()
        borderTop2.borderWidth = 1
        borderTop2.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderTop2.frame = CGRect(x: 0, y: 0, width: login_password.frame.width, height: 1)
        self.login_password.layer.addSublayer(borderTop2)
        self.login_password.layer.masksToBounds = true
        
        let borderBot = CALayer()
        borderBot.borderWidth = 1
        borderBot.borderColor = UIColor(red: textfield_placeholder_red, green: textfield_placeholder_green, blue: textfield_placeholder_blue, alpha: 1).CGColor
        borderBot.frame = CGRect(x: 0, y: login_password.frame.height - 1, width: login_password.frame.width, height: 1)
        self.login_password.layer.addSublayer(borderBot)
        self.login_password.layer.masksToBounds = true
        
        
        // MARK: - Textfield Background Color
        login_username.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        login_password.backgroundColor = UIColor(red: textfield_bg_color_red, green: textfield_bg_color_green, blue: textfield_bg_color_blue, alpha: 1)
        
        // MARK: - Button's Color
        login_back_btn.setTitleColor((UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)), forState: .Normal)
        login_signIn.setTitleColor((UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)), forState: .Normal)
        login_frgtPwd.setTitleColor((UIColor(red: purple_color_red, green: purple_color_green, blue: purple_color_blue, alpha: 1)), forState: .Normal)
        
 
 */
        login_signIn.addTarget(self, action: #selector(login_user), forControlEvents: .TouchUpInside)
        
 
        
        
        ////
        
        
        
        ////
        
    }
    
    
   
    
    
    
    
    
    
    
    
    func post(completion: (message: String?) -> Void) {
        
        let url = NSURL(string:"https://thread-app.herokuapp.com/login")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let parameters : [String:AnyObject] = [ "email" : self.login_username.text!,
                       "password": self.login_password.text!
        ]
        
     //   request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(values, options: [])
        
        
        Alamofire.request(.POST, url!, parameters: parameters, encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                print("Success with JSON: \(JSON)")
                
                let response = JSON as! NSDictionary
              //  let bool_val = (response["status"]!)!
                if(response["status"]! as! NSObject == 1)
                {
                //example if there is an id
                    let userId = (response["data"]!["email"])!
                    let usrId = (response["data"]!["_id"])!
                   usr_id = usrId!
               // let userId = response.objectForKey("data") as! String
                    self.allow_login = userId!
                    print("My id-",usr_id)
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
                
                load_thread()
                completion(message: NSString(data: self.dummy as NSData, encoding: NSUTF8StringEncoding) as? String)
        }
        
        
        
        
        
//            let request = NSMutableURLRequest(URL: NSURL(string: "https://thread-app.herokuapp.com/login")!)
//            request.HTTPMethod = "POST"
//            
//            let postString = ("email="+self.login_username.text!+"&password="+self.login_password.text!)
//            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
//            
//            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
//                data, response, error in
//                if error != nil {
//                    print("error=\(error)")
//                    return
//                }
//                do {
//                    if let convertedJsonIntoDict = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
//                        
//                        // Print out dictionary
//                        print(convertedJsonIntoDict)
//                        
//                        // Get value by key
//                       // self.allow_login = convertedJsonIntoDict["data"]!["email"] as? String
//                       // print(firstNameValue!)
//                        
//                    }
//                } catch let error as NSError {
//                    print(error.localizedDescription)
//                }
//
//                
//                completion(message: NSString(data: data!, encoding: NSUTF8StringEncoding) as? String)
//            }
//        
//        
//            
//            task.resume()
    }
    
    func login_user(Sender : UIButton)
    {
//        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) { [unowned self] in
//            let url:NSURL = NSURL(string: "https://thread-app.herokuapp.com/login")!
//            let session = NSURLSession.sharedSession()
//            
//            let request = NSMutableURLRequest(URL: url)
//            request.HTTPMethod = "POST"
//            request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
//            
//            
//            let data = ("email="+self.login_username.text!+"&password="+self.login_password.text!)
//            
//            let ns_data = data.dataUsingEncoding(NSUTF8StringEncoding)
//            
//            
//            let task = session.uploadTaskWithRequest(request, fromData: ns_data, completionHandler:
//                {(data,response,error) in
//                    
//                    guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
//                        print(data)
//                        return
//                    }
//                    
//                    let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                    print(dataString)
//                }
//               
//                
//            );
//            
//            task.resume()
//        }
//        
//        if(true)
//        {
//            
//        }
        
        indicator.startAnimating()
        login_signIn.enabled = false
        login_signIn.alpha = 0.5
        post(completionHandler)
    }
    
    func completionHandler(message: String?){
       indicator.stopAnimating()
        login_signIn.enabled = true
        login_signIn.alpha = 1
        let defaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setValue(usr_id, forKey: defaultsKeys.local_user_id)
        defaults.synchronize()
    if(allow_login == self.login_username.text)
    {
        dispatch_async(dispatch_get_main_queue()) {
             let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            let downloadingFilePath1 = NSTemporaryDirectory().stringByAppendingString("temp-download")
            let downloadingFileURL1 = NSURL(fileURLWithPath: downloadingFilePath1 )
            //   let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            
            let readRequest1 : AWSS3TransferManagerDownloadRequest = AWSS3TransferManagerDownloadRequest()
            readRequest1.bucket = "metaphor.thread"
            readRequest1.key =  usr_id
            readRequest1.downloadingFileURL = downloadingFileURL1
            
            let task = transferManager.download(readRequest1)
            task.continueWithBlock { (task) -> AnyObject! in
                print(task.error)
                if task.error != nil {
                } else {
                    dispatch_async(dispatch_get_main_queue()
                        , { () -> Void in
                            usr_img = UIImage(contentsOfFile: downloadingFilePath1)!
                            
                    })
                    print("Fetched image")
                }
                return nil
            }
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        
        
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("myTabBar") as! MyTabBarController
        NSOperationQueue.mainQueue().addOperationWithBlock {
            self.presentViewController(nextViewController, animated:true, completion:nil)
    }
        
      }
    }
    
    
    
    
    func adjustForKeyboard(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let keyboardViewEndFrame = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        
        if notification.name == UIKeyboardWillHideNotification {
            Login_yourtextview.contentInset = UIEdgeInsetsZero
        } else {
            Login_yourtextview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        Login_yourtextview.scrollIndicatorInsets = Login_yourtextview.contentInset
        
        //  let selectedRange = 100
        // yourTextView.scrollRangeToVisible(selectedRange)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}